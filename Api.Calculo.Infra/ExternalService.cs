﻿using Api.Calculo.Domain.Interfaces;
using Api.Calculo.Domain.Model;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Api.Calculo.Infra
{
    public class ExternalService : IExternalService
    { 
        public CalculoResposta BuscarTaxaExterno()
        {
            var client = new RestClient("https://localhost:5001");
            var request = new RestRequest("api/Juros/taxaJuros", Method.GET);

            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            var response = client.Execute(request);
            var content = JsonConvert.DeserializeObject<CalculoResposta>(response.Content);

            return content;
        }

    }
}

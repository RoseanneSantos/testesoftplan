﻿using Api.Calculo.Domain.Interfaces;
using Api.Calculo.Domain.Services;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Api.Calculo.Infra
{
    public class InjecaoDependencia
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<ICalculoService, CalculoService>();
            services.AddScoped<IExternalService, ExternalService>();
        }
    }
}

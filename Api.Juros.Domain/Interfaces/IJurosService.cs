﻿using Api.Juros.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Juros.Domain.Interfaces
{
    public interface IJurosService
    {
        JurosDto BuscaUltimaTaxa();

        void GravarTaxaJuros(double taxa);
    }
}

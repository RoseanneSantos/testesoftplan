﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Juros.Domain.Models
{
    public class JurosDto
    {
        public decimal Taxa { get; set; }

        public JurosDto(double taxa)
        {
            var taxPercentage = taxa / 100;
            Taxa = (decimal)taxPercentage;
        }
    }
}

﻿using Api.Juros.Domain.Interfaces;
using Api.Juros.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Juros.Domain.Services
{
    public class JurosService : IJurosService
    {
        private readonly IJurosRepositorio _jurosRepositorio;

        public JurosService(IJurosRepositorio jurosRepositorio)
        {
            _jurosRepositorio = jurosRepositorio;
        }

        public JurosDto BuscaUltimaTaxa()
        {
            var taxa = _jurosRepositorio.BuscaTaxa();

            if (taxa == 0)
                throw new EntryPointNotFoundException();

            var retorno = new JurosDto(taxa);

            return retorno;
        }

        public void GravarTaxaJuros(double taxa)
        {
            if (taxa < 1)
                throw new ArgumentException();

            _jurosRepositorio.GravarTaxa(taxa);

        }
    }
}

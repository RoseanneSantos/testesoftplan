# TesteSoftplan Passos para execução dos serviços

Executar os comandos abaixo no terminal para criação do container sqlserver no docker:

docker pull mcr.microsoft.com/mssql/server:2017-latest

docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=Teste123!" -p 1433:1433 --name sqlteste -d mcr.microsoft.com/mssql/server:2017-latest

docker exec -it sqlteste /opt/mssql-tools/bin/sqlcmd -S localhost -U sa

/////////////////////////////////////////////////////
Em seguida, digitar o comando no terminal:

sqlcmd -S localhost -U SA -P Teste123!

Executar o script de criação da base
CREATE DATABASE ApiJurosDB
SELECT Name from sys.Databases
GO

Executar o script de criação de tabela 
USE ApiJurosDB
GO
CREATE TABLE TaxaJuros(
Id int IDENTITY(1,1) NOT NULL,
Taxa float NOT NULL)

////////////////////////////////////////////////////////
Para executar os dois serviços GET e POST da API de Juros é necessário, executar o projeto ApiTaxaJuros ambos com a mesma rota https://localhost:5001/api/Juros/taxaJuros
porém com protocolos diferentes. 

Para consumir o serviço Calcula Juros que possui a rota https://localhost:5002/api/Calculo/taxaJuros, é necessário que a API Juros esteja sendo executada pelo visual studio. 

Para consumir as Apis informando os valores no Swagger, deve ser feita a criação do container do banco de dados no docker seguindo as intruções acima.


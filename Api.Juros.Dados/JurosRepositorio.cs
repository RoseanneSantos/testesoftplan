﻿using Api.Juros.Domain.Interfaces;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Juros.Dados
{
    public class JurosRepositorio : IJurosRepositorio
    {
        IConfiguration _configuration;
        public JurosRepositorio(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GetConnection()
        {
            var connection = _configuration.GetSection("ConnectionStrings").
            GetSection("TaxaJuros").Value;
            return connection;
        }

        public double BuscaTaxa()
        {
            double resultado;

            using (SqlConnection conexao = new SqlConnection(GetConnection()))
            {
                resultado = conexao.QueryFirstOrDefault<double>(
                     "SELECT Taxa FROM dbo.TaxaJuros " +
                     "order by Id Desc");
            };

            return resultado;
        }

        public void GravarTaxa(double taxa)
        {
            using (SqlConnection conexao = new SqlConnection(GetConnection()))
            {
                conexao.Execute(@"Insert Into TaxaJuros (Taxa) Values(@taxa)", new { taxa });
            };
        }
    }
}

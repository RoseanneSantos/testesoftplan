﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace ApiTaxa.Teste.Configuration
{
    public class TestContext
    {
        public HttpClient Client { get; private set; }

        public TestContext()
        {
            var server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            Client = server.CreateClient();
        }
    }
}

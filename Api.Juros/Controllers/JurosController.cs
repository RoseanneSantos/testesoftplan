﻿using Api.Juros.Domain.Interfaces;
using Api.Juros.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;

namespace Api.Juros.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JurosController : ControllerBase
    {
        private readonly IJurosService _jurosService;

        public JurosController(IJurosService jurosService)
        {
            _jurosService = jurosService;
        }

        /// <summary>
        /// Busca a ultima taxa de juros
        /// </summary>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <returns>Retorna a ultima taxa de juros</returns>
        [HttpGet]
        [Route("taxaJuros")]
        [AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(JurosDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        public IActionResult BuscaUltimoJuros()
        {
            try
            {
                var retornotaxa = _jurosService.BuscaUltimaTaxa();

                return Ok(retornotaxa);
            }
            catch (EntryPointNotFoundException)
            {
                return NotFound("Taxa não encontrada");
            }
            catch(Exception)
            {
                return BadRequest();
            }

        }

        /// <summary>
        /// Grava taxa de juros
        /// </summary>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        [HttpPost]
        [Route("taxaJuros")]
        [AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult GravaTaxaJuros([FromQuery] int taxajuros)
        {
            try
            {
                _jurosService.GravarTaxaJuros(taxajuros);

                return Ok("Taxa Inserida");
            }
            catch (ArgumentException)
            {
                return BadRequest("Taxa inválida");
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}

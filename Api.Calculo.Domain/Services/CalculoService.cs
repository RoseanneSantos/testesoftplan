﻿using Api.Calculo.Domain.Interfaces;
using Api.Calculo.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Calculo.Domain.Services
{
    public class CalculoService : ICalculoService
    {
        private readonly IExternalService _externalService;

        public CalculoService(IExternalService externalService)
        {
            _externalService = externalService;
        }

        public decimal CalcularJuros(decimal valorinicial, int tempo)
        {
            if (valorinicial > 0 && tempo > 0)
            {
                var juros = _externalService.BuscarTaxaExterno();

                string valorfinal = ConversaoCalculo(valorinicial, tempo, juros);

                return decimal.Parse(valorfinal);
            }
            else
            {
                throw new ArgumentException();
            }

        }

        private static string ConversaoCalculo(decimal valorinicial, int tempo, CalculoResposta juros)
        {
            double taxa = double.Parse(Convert.ToString(1 + juros.taxa));

            var resultado = double.Parse(Convert.ToString(valorinicial)) * Math.Pow(taxa, tempo);

            var valorfinal = (Math.Truncate(100 * Convert.ToDecimal(resultado)) / 100).ToString("F");

            return valorfinal;
        }
    }
}

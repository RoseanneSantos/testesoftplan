﻿using Api.Calculo.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Calculo.Domain.Interfaces
{
    public interface IExternalService
    {
        CalculoResposta BuscarTaxaExterno();
    }
}

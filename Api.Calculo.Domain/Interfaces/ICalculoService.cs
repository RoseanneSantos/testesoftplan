﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Calculo.Domain.Interfaces
{
    public interface ICalculoService
    {
        decimal CalcularJuros(decimal valorinicial, int tempo);
    }
}

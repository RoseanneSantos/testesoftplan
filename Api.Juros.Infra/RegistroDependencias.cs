﻿using Api.Juros.Dados;
using Api.Juros.Domain.Interfaces;
using Api.Juros.Domain.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Api.Juros.Infra
{
    public class RegistroDependencias
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IJurosService, JurosService>();
            services.AddScoped<IJurosRepositorio, JurosRepositorio>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Api.Taxa.Testes.Configuration
{
    public class TesteContexto
    {
        public HttpClient Client { get; private set; }

        public TesteContexto()
        {
            var server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            Client = server.CreateClient();
        }
    }
}

﻿using Api.Calculo.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;

namespace Api.CalculoTaxa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalculoController : ControllerBase
    {
        private readonly ICalculoService _calculoService;

        public CalculoController(ICalculoService calculoService)
        {
            _calculoService = calculoService;
        }

        /// <summary>
        /// Grava taxa de juros
        /// </summary>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        [HttpPost]
        [Route("taxaJuros")]
        [AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult CalculaTaxaJuros([FromQuery] decimal valorinicial, [FromQuery] int tempo)
        {
            try
            {
               var valorfinal = _calculoService.CalcularJuros(valorinicial, tempo);

                return Ok(valorfinal);
            }
            catch (ArgumentException)
            {
                return BadRequest("Valores inválidos");
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}

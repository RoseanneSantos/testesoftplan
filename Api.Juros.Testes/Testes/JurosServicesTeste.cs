using System;
using Api.Juros.Domain.Interfaces;
using Api.Juros.Domain.Services;
using FluentAssertions;
using Moq;
using Xunit;

namespace Api.Juros.Testes
{
    public class JurosServicesTeste
    {
        [Theory]
        [InlineData(1, 0.01)]
        [InlineData(2.5, 0.025)]
        public void DeveBuscarUltimaTaxaComSucesso(double taxaFake, decimal taxaResultado)
        {
            //Mocks e cria��o de propriedades
            var repository = new Mock<IJurosRepositorio>();

            IJurosService service = new JurosService(repository.Object);

            repository.Setup(x => x.BuscaTaxa()).Returns(taxaFake);

            //Chamada do servi�o
            var resultado = service.BuscaUltimaTaxa();

            //Valida��o do resultado
            resultado.Should().NotBeNull();
            resultado.Taxa.Should().NotBe(0);
            resultado.Taxa.Should().Be(taxaResultado);

            repository.Verify(x => x.BuscaTaxa(), Times.Once);
        }

        [Fact]
        public void DeveEntrarExcecaoTaxaZero()
        {
            //Mocks e cria��o de propriedades
            var repository = new Mock<IJurosRepositorio>();

            IJurosService service = new JurosService(repository.Object);

            repository.Setup(x => x.BuscaTaxa()).Returns(0);

            //Chamada do servi�o
            Action act = () => service.BuscaUltimaTaxa();

            //assert
            act.Should().ThrowExactly<EntryPointNotFoundException>().WithMessage("Entry point was not found.");

        }

        [Fact]
        public void DeveEntrarExcecaoGravaTaxa()
        {
            //Mocks e cria��o de propriedades
            var repository = new Mock<IJurosRepositorio>();

            IJurosService service = new JurosService(repository.Object);

            repository.Setup(x => x.GravarTaxa(0));

            //Chamada do servi�o
            Action act = () => service.GravarTaxaJuros(0);

            //assert
            act.Should().ThrowExactly<ArgumentException>().WithMessage("Value does not fall within the expected range.");


        }

        [Theory]
        [InlineData(1)]
        [InlineData(2.5)]
        public void DeveGravarTaxaComSucesso(double taxaFake)
        {
            //Mocks e cria��o de propriedades
            var repository = new Mock<IJurosRepositorio>();

            IJurosService service = new JurosService(repository.Object);

            repository.Setup(x => x.GravarTaxa(taxaFake));

            //Chamada do servi�o
            service.GravarTaxaJuros(taxaFake);

            //Valida��o 
            repository.Verify(x => x.GravarTaxa(taxaFake), Times.Once);
        }
    }
}

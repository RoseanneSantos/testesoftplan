﻿using System;
using System.Net;
using System.Threading.Tasks;
using Api.Juros.Controllers;
using Api.Juros.Domain.Interfaces;
using Api.Juros.Domain.Models;
using Api.Juros.Testes.Configuration;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;

namespace Api.Juros.Testes
{
    public class JurosControllerTeste
    {
        public readonly IWebHostBuilder _builder;

        private Mock<IJurosService> _jurosService;

        public JurosControllerTeste()
        {
            _jurosService = new Mock<IJurosService>();

            _builder = new WebHostBuilder();
            Configure(_builder);
        }

        internal void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureAppConfiguration((context, b) => {
                context.HostingEnvironment.ApplicationName = typeof(JurosController).Assembly.GetName().Name;
            });

            builder.ConfigureTestServices(services =>
            {
                services.AddScoped(provider => _jurosService.Object);
            });
        }

        [Fact]
        public async Task BuscarUltimoJuros_ReturnsOkResponse()
        {
            using (var client = new TesteContexto(_builder).Client)
            {
                // Arrange
                var baseUrl = "/api/juros/taxajuros";
                var taxaFake = 1;
                var jurosFake = new JurosDto(taxaFake);

                _jurosService.Setup(x => x.BuscaUltimaTaxa()).Returns(jurosFake);

                //Act
                var response = await client.GetAsync(baseUrl);

                //Assert
                response.EnsureSuccessStatusCode();
                response.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }

        [Fact]
        public async Task BuscarUltimoJuros_ReturnsNotFoundResponse()
        {
            using (var client = new TesteContexto(_builder).Client)
            {
                // Arrange
                var baseUrl = "/api/juros/taxajuros";

                _jurosService.Setup(x => x.BuscaUltimaTaxa()).Throws<EntryPointNotFoundException>();

                //Act
                var response = await client.GetAsync(baseUrl);

                //Assert
                response.StatusCode.Should().Be(HttpStatusCode.NotFound);
            }
        }

        [Fact]
        public async Task BuscarUltimoJuros_ReturnsBadRequestResponse()
        {
            using (var client = new TesteContexto(_builder).Client)
            {
                // Arrange
                var baseUrl = "/api/juros/taxajuros";

                _jurosService.Setup(x => x.BuscaUltimaTaxa()).Throws<Exception>();

                //Act
                var response = await client.GetAsync(baseUrl);

                //Assert
                response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            }
        }

        [Fact]
        public async Task GravaTaxaJuros_ReturnsOkResponse()
        {
            using (var client = new TesteContexto(_builder).Client)
            {
                //Arrange
                var taxa = 1;
                var baseUrl = $"/api/juros/taxaJuros?taxajuros={taxa}";

                _jurosService.Setup(x => x.GravarTaxaJuros(taxa));

                //Act
                var response = await client.PostAsync(baseUrl, null);

                //Assert
                response.EnsureSuccessStatusCode();
                response.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }

        [Fact]
        public async Task GravaTaxaJuros_ReturnsArgumentResponse()
        {
            using (var client = new TesteContexto(_builder).Client)
            {
                //Arrange
                var taxa = 1;
                var baseUrl = $"/api/juros/taxaJuros?taxajuros={taxa}";

                _jurosService.Setup(x => x.GravarTaxaJuros(taxa)).Throws<ArgumentException>();

                //Act
                var response = await client.PostAsync(baseUrl, null);

                //Assert
                response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            }
        }

        [Fact]
        public async Task GravaTaxaJuros_ReturnsBadRequestResponse()
        {
            using (var client = new TesteContexto(_builder).Client)
            {
                //Arrange
                var taxa = 1;
                var baseUrl = $"/api/juros/taxaJuros?taxajuros={taxa}";

                _jurosService.Setup(x => x.GravarTaxaJuros(taxa)).Throws<Exception>();

                //Act
                var response = await client.PostAsync(baseUrl, null);

                //Assert
                response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            }
        }
    }
}

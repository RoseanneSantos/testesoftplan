﻿using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace Api.Juros.Testes.Configuration
{
    public class TesteContexto
    {
        public HttpClient Client { get; private set; }

        public TesteContexto(IWebHostBuilder builder)
        {
            var server = new TestServer(builder.UseStartup<Startup>());
            Client = server.CreateClient();
        }
    }
}

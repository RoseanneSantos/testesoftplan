﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Api.CalculoTaxa;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace Api.Calculo.Testes.Configuration
{
    public class TesteContexto
    {
        public HttpClient Client { get; private set; }

        public TesteContexto(IWebHostBuilder builder)
        {
            var server = new TestServer(builder.UseStartup<Startup>());
            Client = server.CreateClient();
        }
    }
}

using System;
using Api.Calculo.Domain.Interfaces;
using Api.Calculo.Domain.Model;
using Api.Calculo.Domain.Services;
using FluentAssertions;
using Moq;
using Xunit;

namespace Api.Calculo.Testes
{
    public class CalculoServiceTeste
    {
        [Theory]
        [InlineData(100, 5, 105.10)]
        public void DeveCalcularJurosComSucesso(decimal valorInicialFake, int tempoFake, decimal resultadoFinal)
        {
            //Mocks e cria��o de propriedades
            var externalService = new Mock<IExternalService>();
            var juros = new CalculoResposta { taxa = 0.01M };

            ICalculoService service = new CalculoService(externalService.Object);

            externalService.Setup(x => x.BuscarTaxaExterno()).Returns(juros);

            //Chamada do servi�o
            var resultado = service.CalcularJuros(valorInicialFake, tempoFake);

            //Valida��o do resultado
            resultado.Should().NotBe(0);
            resultado.Should().Be(resultadoFinal);
        }

        [Theory]
        [InlineData(0, 0)]
        public void CalcularJuros_Argument(decimal valorInicialFake, int tempoFake)
        {
            //Mocks e cria��o de propriedades
            var externalService = new Mock<IExternalService>();
            var juros = new CalculoResposta { taxa = 0.01M };

            ICalculoService service = new CalculoService(externalService.Object);

            externalService.Setup(x => x.BuscarTaxaExterno()).Returns(juros);

            //Chamada do servi�o
            Action act = () => service.CalcularJuros(valorInicialFake, tempoFake);

            //assert
            act.Should().ThrowExactly<ArgumentException>().WithMessage("Value does not fall within the expected range.");
        }
    }
}

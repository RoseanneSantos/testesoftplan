﻿using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Api.Calculo.Domain.Interfaces;
using Api.Calculo.Testes.Configuration;
using Api.CalculoTaxa.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;

namespace Api.Calculo.Testes
{
    public class CalculoControllerTeste
    {
        public readonly IWebHostBuilder _builder;

        private Mock<ICalculoService> _calculoService;

        public CalculoControllerTeste()
        {
            _calculoService = new Mock<ICalculoService>();

            _builder = new WebHostBuilder();
            Configure(_builder);
        }

        internal void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureAppConfiguration((context, b) => {
                context.HostingEnvironment.ApplicationName = typeof(CalculoController).Assembly.GetName().Name;
            });

            builder.ConfigureTestServices(services =>
            {
                services.AddScoped(provider => _calculoService.Object);
            });
        }


        [Fact]
        public async Task CalculaTaxaJuros_ReturnsOkResponse()
        {
            using (var client = new TesteContexto(_builder).Client)
            {
                //Arrange
                var valorinicial = 100;
                var tempo = 5;
                var retorno = 100.5M;
                var baseUrl = $"/api/calculo/taxaJuros?valorinicial={valorinicial}&tempo={tempo}";

                _calculoService.Setup(x => x.CalcularJuros(valorinicial, tempo)).Returns(retorno);

                //Act
                var response = await client.PostAsync(baseUrl, null);

                //Assert
                response.EnsureSuccessStatusCode();
                response.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }

        [Fact]
        public async Task CalculaTaxaJuros_ReturnsArgumentResponse()
        {
            using (var client = new TesteContexto(_builder).Client)
            {
                //Arrange
                var valorinicial = 100;
                var tempo = 5;
                var baseUrl = $"/api/calculo/taxaJuros?valorinicial={valorinicial}&tempo={tempo}";

                _calculoService.Setup(x => x.CalcularJuros(valorinicial, tempo)).Throws<ArgumentException>();

                //Act
                var response = await client.PostAsync(baseUrl, null);

                //Assert
                 response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            }
        }

        [Fact]
        public async Task CalculaTaxaJuros_ReturnsBadRequestResponse()
        {
            using (var client = new TesteContexto(_builder).Client)
            {
                //Arrange
                var valorinicial = 100;
                var tempo = 5;
                var baseUrl = $"/api/calculo/taxaJuros?valorinicial={valorinicial}&tempo={tempo}";

                _calculoService.Setup(x => x.CalcularJuros(valorinicial, tempo)).Throws<Exception>();

                //Act
                var response = await client.PostAsync(baseUrl, null);

                //Assert
                response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            }
        }
    }
}
